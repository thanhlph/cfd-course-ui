/* eslint-disable no-constant-condition */
/* eslint-disable no-useless-escape */
/* eslint-disable react/prop-types */
/* eslint-disable no-unused-vars */
import React from "react";

const Select = ({ options, error, ...rest }) => {
  return (
    <select {...rest} className={`form__input ${error ? "formerror" : ""}`}>
      {options?.map((option, index) => {
        return (
          <option key={option?.value || index} value={option?.value}>
            {option?.label || ""}
          </option>
        );
      })}
    </select>
  );
};

export default Select;
