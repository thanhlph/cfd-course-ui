/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import React from "react";
import { Link } from "react-router-dom";
import moment from "moment";
import PATHS from "../../constants/path";
import { ROLES } from "../../constants/role";
import { COURSE_ITEM_TYPE } from "../../constants/general";
import { formatCurrency, formatDate } from "../../utils/format";
import Button from "../Button";

const CourseItem = ({
  type = COURSE_ITEM_TYPE.normal,
  image,
  slug,
  name,
  teams,
  startDate,
  tags,
  price,
}) => {
  const courseDetailPath = PATHS.COURSE.INDEX + `/${slug}`;
  const courseOrderPath = "/course-order" + `/${slug}`;
  const teacherInfo = teams?.find((item) => item.tags.includes(ROLES.teacher));

  if (type === COURSE_ITEM_TYPE.normal) {
    return (
      <div className="courses__list-item">
        <div className="img">
          <Link to={courseDetailPath}>
            <img src={image} alt="Khóa học CFD" className="course__thumbnail" />
            {tags?.length > 0 && (
              <span className="course__img-badge badge">
                {tags.join(" | ")}
              </span>
            )}
          </Link>
        </div>
        <div className="content">
          <p className="label">Front-End</p>
          <h3 className="title --t3">
            <Link to={courseDetailPath}>{name}</Link>
          </h3>
          <div className="content__info">
            {teacherInfo && (
              <div className="user">
                <div className="user__img">
                  <img src={teacherInfo.image || ""} alt="Avatar teacher" />
                </div>
                <p className="user__name">{teacherInfo.name || ""}</p>
              </div>
            )}

            <div className="price">
              <strong>{formatCurrency(price)} đ</strong>
            </div>
          </div>
          {/* <div iv className="content__action">
            <Button link={courseOrderPath}>Đăng ký ngay</Button>
            <Link to={courseDetailPath} className="btn btn--default">
              <img src="/img/icon-paper.svg" alt="icon paper" />
            </Link>
          </div> */}
        </div>
      </div>
    );
  }

  return (
    <div className="coursecoming__item">
      <div className="coursecoming__item-img">
        <Link to={courseDetailPath}>
          <img src={image || []} alt="Khóa học sắp ra mắt CFD" />
        </Link>
      </div>
      <div className="coursecoming__item-content">
        <p className="category label">Front-end</p>
        <h2 className="title --t2">
          <Link to={courseDetailPath}>{name}</Link>
        </h2>
        {teacherInfo?.id && (
          <div className="user">
            <div className="user__img">
              <img src={teacherInfo.image || ""} alt="Avatar teacher" />
            </div>
            <p className="user__name">{teacherInfo.name || ""}</p>
          </div>
        )}

        <div className="info">
          {startDate && (
            <div className="labeltext">
              <span className="label --blue">Ngày khai giảng</span>
              <p className="title --t2">{formatDate(startDate)}</p>
            </div>
          )}
          {tags?.length > 0 && (
            <div className="labeltext">
              <span className="label --blue">Hình thức học</span>
              <p className="title --t2">{tags.join(" | ")}</p>
            </div>
          )}
        </div>
        <div className="btnwrap">
          <Button link={courseOrderPath}>Đăng Ký Học</Button>
          <Button link={courseDetailPath} variant="border">
            Xem chi tiết
          </Button>
        </div>
      </div>
    </div>
  );
};

export default CourseItem;
