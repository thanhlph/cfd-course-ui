/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import { Empty } from "antd";
import React, { useState } from "react";

const Accordion = ({ data = [], label = "" }) => {
  const [activeId, setActiveId] = useState("");
  const _onTitleClick = (e, id) => {
    e.stopPropagation();
    setActiveId(id !== activeId ? id : "");
  };

  return (
    <>
      <div className="accordion">
        {!!label && <h3 className="accordion_title label">{label}</h3>}
        {data?.length > 0 ? (
          data.map((item, index) => {
            const { id, title, content } = item || {};
            return (
              <div
                className={`accordion__content ${
                  activeId === id ? "active" : ""
                }`}
                key={item.id || index}
              >
                <div
                  className="accordion__content-title"
                  onClick={(e) => _onTitleClick(e, id)}
                >
                  <h4>
                    <strong>{item.title || ""}</strong>
                  </h4>
                </div>
                <div className="accordion__content-text">
                  {item.content || ""}
                </div>
              </div>
            );
          })
        ) : (
          <Empty
            description="Không có nội dung hiển thị"
            style={{ margin: "0 auto" }}
          />
        )}
      </div>
    </>
  );
};

export default Accordion;
