import React, { useEffect } from "react";
import { Navigate, Outlet, useNavigate } from "react-router-dom";
import { MODAL_TYPE } from "../../constants/general";
import { useAuthContext } from "../../context/AuthenContext";
import tokenMethod from "../../utils/token";

const PrivateRoute = (redirectPath = "") => {
  const { handleShowModal } = useAuthContext();
  const navigate = useNavigate();

  useEffect(() => {
    if (!tokenMethod.get()) {
      handleShowModal?.(MODAL_TYPE.login);
    }
  }, [handleShowModal]);

  if (!tokenMethod.get()) {
    if (redirectPath) {
      return <Navigate to={redirectPath} />;
    } else {
      navigate(-1);
    }
  }
  return <Outlet />;
};

export default PrivateRoute;
