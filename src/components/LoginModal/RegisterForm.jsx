/* eslint-disable no-unused-vars */
import React, { useState } from "react";
import { Link } from "react-router-dom";
import { useAuthContext } from "../../context/AuthenContext";
import { MODAL_TYPE } from "../../constants/general";
import PATHS from "../../constants/path";
import { regexRule, requireRule } from "../../utils/validate";
import useForm from "../../hooks/useForm";
import Input from "../Input";
import Button from "../Button";
import ComponentLoading from "../ComponentLoading";

const RegisterForm = () => {
  const [loading, setLoading] = useState(false);
  const { handleShowModal, handleCloseModal, handleRegister } =
    useAuthContext();

  const { form, register, validate } = useForm(
    {
      name: "",
      email: "",
      password: "",
      confirmPassword: "",
    },
    {
      name: [requireRule("Vui lòng nhập name")],
      email: [
        requireRule("Vui lòng nhập email"),
        regexRule("Vui lòng nhập đúng định dạng email", "email"),
      ],
      password: [requireRule("Vui lòng nhập password")],
      confirmPassword: [
        requireRule("Vui lòng nhập confirm password"),
        (value, values) => {
          if (values.password && value !== values.password) {
            return "Mật khẩu xác nhận không đúng";
          }
          return false;
        },
      ],
    }
  );

  const _onSubmit = (e) => {
    e.preventDefault();
    const errorObject = validate();
    if (Object.keys(errorObject).length > 0) {
      console.log("Submit Error", errorObject);
    } else {
      setLoading(true);
      if (typeof handleRegister === "function") {
        handleRegister(form, () => {
          setLoading(false);
        });
      }
    }
  };

  return (
    <div
      className="modal__wrapper-content mdregister active"
      style={{ position: "relative" }}
    >
      {loading && <ComponentLoading />}
      <div className="form__bottom">
        <p>Bạn đã có tài khoản?</p>
        <div
          className="color--primary btnmodal"
          data-modal="mdlogin"
          onClick={() => handleShowModal(MODAL_TYPE.login)}
        >
          <strong>Đăng nhập</strong>
        </div>
      </div>
      <form onSubmit={_onSubmit} className="form">
        <Input
          label="Họ và tên"
          placeholder="Name"
          required
          {...register("name")}
        />
        <Input
          label="Email"
          placeholder="Email"
          required
          {...register("email")}
        />
        <Input
          label="Mật khẩu"
          placeholder="Mật khẩu"
          required
          type="password"
          {...register("password")}
        />
        <Input
          label="Xác nhận mật khẩu"
          placeholder="Xác nhận mật khẩu"
          required
          type="password"
          {...register("confirmPassword")}
        />
        {/* <div className="form-group">
          <input
            defaultValue
            type="text"
            className="form__input formerror"
            placeholder="Họ và tên"
          />
          <p className="error">Họ và tên không được để trống</p>
        </div>
        <div className="form-group">
          <input
            defaultValue
            type="email"
            className="form__input"
            placeholder="Email"
          />
        </div>
        <div className="form-group">
          <input
            defaultValue
            type="password"
            className="form__input"
            placeholder="Mật khẩu"
          />
        </div>
        <div className="form-group">
          <input
            defaultValue
            type="password"
            className="form__input"
            placeholder="Xác nhận mật khẩu"
          />
        </div> */}
        <p className="form__argee">
          Với việc đăng ký, bạn đã đồng ý
          <Link
            className="color--primary"
            to={PATHS.PRIVACY}
            onClick={handleCloseModal}
          >
            Chính Sách Điều Khoản
          </Link>{" "}
          của CFD
        </p>
        <Button className="form__btn-register" type="submit">
          Đăng ký tài khoản
        </Button>
      </form>
    </div>
  );
};

export default RegisterForm;
