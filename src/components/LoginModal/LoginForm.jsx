/* eslint-disable no-unused-vars */
import React, { useState } from "react";
import useForm from "../../hooks/useForm";
import { regexRule, requireRule } from "../../utils/validate";
import Input from "../Input/index";
import Button from "../Button/index";
import { useAuthContext } from "../../context/AuthenContext";
import { MODAL_TYPE } from "../../constants/general";
import ComponentLoading from "../ComponentLoading";
import { message } from "antd";

const LoginForm = () => {
  const [loading, setLoading] = useState(false);
  const { handleShowModal, handleLogin } = useAuthContext();
  const { form, register, validate } = useForm(
    {
      email: "",
      password: "",
    },
    {
      email: [
        requireRule("Vui lòng nhập email"),
        regexRule("Vui lòng nhập đúng định dạng email", "email"),
      ],
      password: [requireRule("Vui lòng nhập mật khẩu")],
    }
  );

  const _onSubmit = (e) => {
    e.preventDefault();
    const errorObject = validate();
    if (Object.keys(errorObject).length > 0) {
      console.log("submit Error", errorObject);
    } else {
      setLoading(true);
      console.log("submit Success :>> ", form);
      handleLogin?.(form, () => {
        setTimeout(() => {
          setLoading(false);
        }, 300);
      });
    }
  };

  return (
    <div
      className="modal__wrapper-content mdlogin active"
      style={{ position: "relative" }}
    >
      {loading && <ComponentLoading />}
      <div className="form__bottom">
        <p>Bạn chưa có tài khoản?</p>
        <div
          className="color--primary btnmodal"
          data-modal="mdregister"
          onClick={() => handleShowModal(MODAL_TYPE.register)}
        >
          <strong>Đăng ký</strong>
        </div>
      </div>
      <form onSubmit={_onSubmit} className="form">
        <Input
          label="Email"
          placeholder="Email"
          required
          {...register("email")}
        />
        <Input
          type="password"
          label="Password"
          placeholder="Password"
          required
          {...register("password")}
        />
        {/* <div className="form-group">
          <input
            defaultValue
            type="email"
            className="form__input formerror"
            placeholder="Email"
          />
          <p className="error">Email không được để trống</p>
        </div>
        <div className="form-group">
          <input
            defaultValue
            type="password"
            className="form__input"
            placeholder="Mật khẩu"
          />
        </div> */}
        <Button className="form__btn-register" type="submit">
          Đăng nhập
        </Button>
      </form>
    </div>
  );
};

export default LoginForm;
