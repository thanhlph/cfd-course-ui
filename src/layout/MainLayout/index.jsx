/* eslint-disable no-unused-vars */
import React from "react";
import { Outlet } from "react-router-dom";
import Footer from "../../components/Footer";
import Header from "../../components/Header";
import Navbar from "../../components/Navbar";
import Overlay from "../../components/Overlay";
import PageLoading from "../../components/PageLoading";
import LoginModal from "../../components/LoginModal";
import MainContextProvider from "../../context/MainContext";
import AuthenContextProvider from "../../context/AuthenContext";

const MainLayout = () => {
  return (
    <MainContextProvider>
      <AuthenContextProvider>
        <PageLoading />
        <Header />
        <Navbar />
        <Overlay />
        {/* Main */}
        <Outlet />
        <Footer />
        {/* Modal Đăng Nhập / Đăng Ký */}
        <LoginModal />
      </AuthenContextProvider>
    </MainContextProvider>
  );
};

export default MainLayout;
