import React, { useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import useMutation from "../../hooks/useMutation";
import { courseService } from "../../services/courseService";
import FormOrder from "./FormOrder";
import InfoOrder from "./InfoOrder";
import PaymentOrder from "./PaymentOrder";
import Button from "../../components/Button";
import { useEffect } from "react";
import { formatCurrency } from "../../utils/format";
import { useAuthContext } from "../../context/AuthenContext";
import useForm from "../../hooks/useForm";
import { requireRule, regexRule } from "../../utils/validate";
import { message } from "antd";
import { orderService } from "../../services/orderService";
import PATHS from "../../constants/path";

const CourseOrderPage = () => {
  // Chuan bi data cho InfoOrder
  const { courseSlug } = useParams();
  const navigate = useNavigate();

  const { data: courseDetailData, execute: executeCourseDetail } = useMutation(
    courseService.getCourseBySlug
  );
  useEffect(() => {
    if (courseSlug) executeCourseDetail(courseSlug, {});
  }, [courseSlug]);

  const { teams, price, tags } = courseDetailData || {};

  // Child props
  const InfoOrderProps = {
    ...courseDetailData,
    teacherInfo: teams?.find((item) => item.tags.includes("Teacher")) || {},
    price: formatCurrency(price),
  };

  // Chuan bi data cho FormOrder
  const {
    profile,
    courseInfo,
    handleGetProfileCourse,
    handleGetProfilePayment,
  } = useAuthContext();
  const isAlreadyOrder =
    courseInfo?.some((item) => item?.course?.slug === courseSlug) || false;
  const {
    firstName: profileName,
    email: profileEmail,
    phone: profilePhone,
  } = profile || {};

  // Handle profile form
  const { form, register, validate, setForm } = useForm(
    {
      name: "",
      email: "",
      phone: "",
      type: "",
    },
    {
      name: [requireRule("Vui lòng nhập tên")],
      email: [
        requireRule("Vui lòng nhập email"),
        regexRule("Vui lòng nhập đúng định dạng email", "email"),
      ],
      phone: [
        requireRule("Vui lòng nhập phone"),
        regexRule("Vui lòng nhập đúng định dạng phone", "phone"),
      ],
      type: [requireRule("Vui lòng chọn hình thức học")],
    }
  );
  useEffect(() => {
    if (isAlreadyOrder && courseInfo?.length > 0) {
      const orderedCourse = courseInfo?.find(
        (item) => item?.course?.slug === courseSlug
      );
      setForm({
        name: orderedCourse.name || "",
        email: profileEmail || "",
        phone: orderedCourse.phone || "",
        type: orderedCourse.type || "",
      });
      setPaymentMethod(orderedCourse.paymentMethod);
    } else {
      setForm({
        name: profileName,
        email: profileEmail,
        phone: profilePhone,
        type: "",
      });
    }
  }, [profileName, profileEmail, profilePhone, isAlreadyOrder, courseInfo]);

  // Payment Order Data
  const [paymentMethod, setPaymentMethod] = useState("");
  const handlePaymentMethodChange = (payment) => {
    setPaymentMethod(payment);
  };

  // Handle orderCourse
  const { loading: orderLoading, execute: orderCourse } = useMutation(
    orderService.orderCourse
  );

  const _onOrder = () => {
    const profileError = validate();

    if (Object.keys(profileError).length > 0) {
      console.log("profileError", profileError);
    } else {
      if (paymentMethod) {
        const payload = {
          name: form.name,
          phone: form.phone,
          course: courseDetailData?.id,
          type: form.type,
          paymentMethod,
        };
        // console.log("payload", payload);
        orderCourse(payload, {
          onSuccess: async () => {
            await handleGetProfileCourse();
            await handleGetProfilePayment();
            navigate(PATHS.PROFILE.MY_COURSE);
            message.success("Đăng ký thành công");
          },
          onFail: () => {
            message.error("Đăng ký thất bại");
          },
        });
      } else {
        message.error("Vui lòng chọn phương thức thanh toán");
      }
    }
  };

  return (
    <>
      <main className="mainwrapper --ptop">
        <section className="sccourseorder">
          <div className="container small">
            <InfoOrder {...InfoOrderProps} />
            <FormOrder
              register={register}
              types={tags}
              disabled={isAlreadyOrder}
            />
            <PaymentOrder
              handleChange={handlePaymentMethodChange}
              selectedPayment={paymentMethod}
              disabled={isAlreadyOrder}
            />
            {/* addclass --processing khi bấm đăng ký */}
            <Button
              onClick={_onOrder}
              disabled={isAlreadyOrder}
              loading={orderLoading}
              style={{ width: "100%" }}
            >
              <span>{isAlreadyOrder ? "Đã đăng ký" : "Đăng ký khoá học"}</span>
            </Button>
          </div>
        </section>
      </main>
    </>
  );
};

export default CourseOrderPage;
