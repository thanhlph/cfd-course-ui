import React, { useEffect } from "react";
import { useParams } from "react-router-dom";
import useDebounce from "../../hooks/useDebounce";
import useMutation from "../../hooks/useMutation";
import useQuery from "../../hooks/useQuery";
import { courseService } from "../../services/courseService";
import { questionService } from "../../services/questionService";
import { formatCurrency, formatDate } from "../../utils/format";
import ContentDetailSection from "./ContentDetailSection";
import CourseSection from "./CourseSection";
import FaqSection from "./FaqSection";
import FeaturedSection from "./FeaturedSection";
import HeroSection from "./HeroSection";
import PageLoading from "../../components/PageLoading/index";
import HeaderTop from "./HeaderTop";

const CourseDetailPage = () => {
  const params = useParams();
  const { courseSlug } = params;

  const { data: questionsData, loading: questionsLoading } = useQuery(
    questionService.getQuestion
  );
  const { data: courseData, loading: coursesLoading } = useQuery(
    courseService.getCourses
  );

  // Lay chi tiet khoa hoc
  const {
    data: courseDetailData,
    loading: courseDetailLoading,
    execute,
  } = useMutation(courseService.getCourseBySlug);

  useEffect(() => {
    if (courseSlug) execute(courseSlug || "");
  }, [courseSlug]);

  // Modify data
  const questions = questionsData?.data?.questions || [];
  const courses = courseData?.data?.courses || [];

  const { teams, startDate, price } = courseDetailData || {};
  const orderLink = `/course-order/` + courseSlug;

  const modifiedProps = {
    ...courseDetailData,
    teacherInfo: teams?.find((item) => item.tags.includes("Teacher")),
    startDate: formatDate(startDate || ""),
    price: formatCurrency(price),
    orderLink,
  };

  const apiLoading = courseDetailLoading || questionsLoading || coursesLoading;
  const pageLoading = useDebounce(apiLoading, 500);

  if (pageLoading) {
    return <PageLoading />;
  }

  return (
    <>
      <HeaderTop {...modifiedProps} />
      <main className="mainwrapper coursedetailpage">
        <HeroSection {...modifiedProps} />
        <ContentDetailSection {...modifiedProps} />
        <FeaturedSection {...modifiedProps} />
        <FaqSection questions={questions} loading={questionsLoading} />
        <CourseSection courses={courses} loading={coursesLoading} />
      </main>
    </>
  );
};

export default CourseDetailPage;
