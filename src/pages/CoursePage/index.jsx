import React from "react";
import useQuery from "../../hooks/useQuery";
import useDebounce from "../../hooks/useDebounce";
import { Empty, Skeleton } from "antd";
import { courseService } from "../../services/courseService";
import CourseItem from "../../components/CourseItem";

const CoursePage = () => {
  const { data, loading: apiLoading } = useQuery(courseService.getCourses);
  const courses = data?.data?.courses || [];

  const loading = useDebounce(apiLoading, 1000);

  return (
    <>
      <main className="mainwrapper courses --ptop">
        <div className="container">
          <div className="textbox">
            <div className="container">
              <h2 className="title --t2">Tất cả khoá học</h2>
            </div>
          </div>
          <div className="courses__list">
            {!loading && courses?.length === 0 && (
              <Empty
                description="Không tìm thấy dữ liệu nào"
                style={{ margin: "0 auto" }}
              />
            )}
            {loading &&
              Array(4)
                .fill("")
                .map((_, index) => (
                  <div
                    key={index}
                    className="courses__list-item"
                    style={{ height: "50vh" }}
                  >
                    <Skeleton active />
                    <br />
                    <Skeleton active />
                  </div>
                ))}
            {!loading &&
              courses?.length > 0 &&
              courses.map((course, index) => {
                return <CourseItem key={course?.id || index} {...course} />;
              })}
          </div>
        </div>
      </main>
    </>
  );
};

export default CoursePage;
