/* eslint-disable react/no-unescaped-entities */
/* eslint-disable no-unused-vars */
import React from "react";
import useQuery from "../../hooks/useQuery";
import { courseService } from "../../services/courseService";
import { teamService } from "../../services/teamService";
import { questionService } from "../../services/questionService";
import { galleryService } from "../../services/galleryService";
import HeroSection from "./HeroSection";
import CourseComingSection from "./CourseComingSection";
import CoursesSection from "./CoursesSection";
import TeacherSection from "./TeacherSection";
import FeaturedSection from "./FeaturedSection";
import TestimonialSection from "./TestimonialSection";
import FaqSection from "./FaqSection";
import GallerySection from "./GallerySection";
import CallRegisterSection from "./CallRegisterSection";

const HomePage = () => {
  const { data: coursesData, loading: coursesLoading } = useQuery(
    courseService.getCourses
  );
  const courses = coursesData?.data.courses || [];

  const comingCourses =
    courses.filter((course) => {
      return course?.startDate && new Date(course.startDate) > new Date();
    }) || [];

  const { data: teamsData, loading: teamsLoading } = useQuery(
    teamService.getTeams
  );

  const teams = teamsData?.data.teams || [];

  const { data: questionData, loading: questionLoading } = useQuery(
    questionService.getQuestion
  );

  const questions = questionData?.data.questions || [];

  const { data: galleriesData, loading: galleriesLoading } = useQuery(
    galleryService.getGallery
  );
  const galleries = galleriesData?.data?.galleries?.[0].images || [];

  return (
    <>
      <main className="mainwrapper">
        <HeroSection />
        <CourseComingSection courses={comingCourses} loading={coursesLoading} />
        <CoursesSection courses={courses} loading={coursesLoading} />
        <TeacherSection teams={teams} loading={teamsLoading} />
        <FeaturedSection />
        {/* --------------------------------Testimonial-------------------------------- */}
        <TestimonialSection />
        {/* --------------------------------faq-------------------------------- */}
        <FaqSection questions={questions} loading={questionLoading} />
        <GallerySection galleries={galleries} loading={galleriesLoading} />
        <CallRegisterSection />
      </main>
    </>
  );
};

export default HomePage;
