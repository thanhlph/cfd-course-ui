import React, { useEffect } from "react";
import { requireRule, regexRule } from "../../utils/validate";
import { useAuthContext } from "../../context/AuthenContext";
import useForm from "../../hooks/useForm";
import Input from "../../components/Input";
import Button from "../../components/Button";
import TextArea from "../../components/TextArea";

const rules = {
  firstName: [requireRule("Vui lòng nhập name")],
  email: [
    requireRule("Vui lòng nhập email"),
    regexRule("Vui lòng nhập đúng định dạng email", "email"),
  ],
  phone: [
    requireRule("Vui lòng nhập phone"),
    regexRule("Vui lòng nhập đúng định dạng phone", "phone"),
  ],
  password: [requireRule("Vui lòng nhập password")],
};

const MyInfo = () => {
  const { profile, handleUpdateProfile } = useAuthContext();
  const { firstName, phone, email } = profile || {};
  const { form, setForm, register, validate } = useForm(
    {
      firstName: "",
      email: "",
      phone: "",
      password: "********",
      facebookURL: "",
      website: "",
      introduce: "",
    },
    rules
  );
  const _onSubmit = (e) => {
    e.preventDefault();
    const errorObject = validate();
    if (Object.keys(errorObject).length > 0) {
      console.log("Submit Error :>> ", errorObject);
    } else {
      handleUpdateProfile?.(form);
    }
  };

  useEffect(() => {
    if (profile) {
      setForm({ ...form, ...profile });
    }
  }, [profile]);

  return (
    <div className="tab__content-item" style={{ display: "block" }}>
      <form action="#" className="form">
        <div className="form-container">
          <Input
            label="Họ và tên"
            defaultValue={firstName}
            required
            placeholder="Họ và tên"
            {...register("firstName")}
          />
          <Input
            label="Số điện thoại"
            defaultValue={phone}
            required
            placeholder="Số điện thoại"
            {...register("phone")}
          />
        </div>
        <div className="form-container">
          <Input
            label="Email"
            defaultValue={email}
            required
            disabled
            placeholder="Email"
            {...register("email")}
          />
          <Input label="Mật khảu" required disabled {...register("password")} />
        </div>
        <Input
          label="Facebook URL"
          defaultValue={email}
          placeholder="facebook URL"
          {...register("facebookURL")}
        />
        <Input
          label="website"
          defaultValue={email}
          placeholder="website"
          {...register("website")}
        />
        <Input
          label="Giới thiệu bản thân"
          renderInput={(inputProps) => {
            return <TextArea {...inputProps} />;
          }}
          {...register("introduce")}
        />
        <Button style={{ width: "100%" }} variant="primary" onClick={_onSubmit}>
          Lưu lại
        </Button>
      </form>
    </div>
  );
};

export default MyInfo;
