import { Empty } from "antd";
import React from "react";
import CourseItem from "../../components/CourseItem";
import { COURSE_ITEM_TYPE } from "../../constants/general";
import { useAuthContext } from "../../context/AuthenContext";
import { formatCurrency } from "../../utils/format";
const MyCourse = () => {
  const { courseInfo } = useAuthContext();
  console.log("courseInfo", courseInfo);

  return (
    <div className="tab__content-item" style={{ display: "block" }}>
      <div className="courses__list">
        {!courseInfo.length && (
          <Empty
            description="Không tìm thấy dữ liệu"
            style={{ margin: "0 auto" }}
          />
        )}
        {courseInfo.length &&
          courseInfo.map((item, index) => {
            return (
              <CourseItem
                key={item?.id || index}
                type={COURSE_ITEM_TYPE.normal}
                {...item?.course}
              />
            );
          })}
      </div>
    </div>
  );
};

export default MyCourse;
