import { Empty } from "antd";
import React from "react";
import { useAuthContext } from "../../context/AuthenContext";
import {
  formatDate,
  formatCurrency,
  PAYMENT_METHOD_LABELS,
} from "../../utils/format";
const MyPayment = () => {
  const { paymentInfo } = useAuthContext();

  return (
    <div className="tab__content-item" style={{ display: "block" }}>
      {!paymentInfo.length && (
        <Empty
          description="Không tìm thấy dữ liệu"
          style={{ margin: "0 auto" }}
        />
      )}
      {paymentInfo.length &&
        paymentInfo.map((item, index) => {
          const { paymentMethod } = item;
          const paymentMethodName = PAYMENT_METHOD_LABELS[paymentMethod];
          return (
            <div className="itemhistory" key={item.id || index}>
              <div className="name">{item.course.name}</div>
              <div className="payment">{paymentMethodName}</div>
              <div className="date">{formatDate(item.createdAt)}</div>
              <div className="money">
                {formatCurrency(item.course.price)} VND
              </div>
            </div>
          );
        })}
    </div>
  );
};

export default MyPayment;
