/* eslint-disable no-unused-vars */
import React, { useState } from "react";
import ContactForm from "./ContactForm";
import ContactSidebar from "./ContactSidebar";
import ContactTitle from "./ContactTitle";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import useMutation from "../../hooks/useMutation";
import PATHS from "../../constants/path";
import { subscribesService } from "../../services/subscribesService";

const ContactPage = () => {
  const navigate = useNavigate();

  const { execute, data, error, loading } = useMutation(
    subscribesService.subscribes
  );

  const handleFormSubmit = async (submitValue) => {
    const payload = {
      name: submitValue.name || "",
      title: submitValue.topic || "",
      email: submitValue.email || "",
      description: submitValue.content || "",
      phone: submitValue.phone || "",
    };
    execute?.(payload, {
      onSuccess: (data) => {
        console.log("data", data);
        navigate(PATHS.HOME);
      },
      onFail: (error) => {
        console.log("error :>> ", error);
      },
    });
  };

  return (
    <main className="mainwrapper contact --ptop">
      <div className="container">
        <ContactTitle />
      </div>
      <div className="contact__content">
        <div className="container">
          <div className="wrapper">
            <ContactSidebar />
            <ContactForm handleFormSubmit={handleFormSubmit} />
          </div>
        </div>
      </div>
    </main>
  );
};

export default ContactPage;
