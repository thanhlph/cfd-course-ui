/* eslint-disable no-constant-condition */
/* eslint-disable no-useless-escape */
/* eslint-disable react/prop-types */
/* eslint-disable no-unused-vars */
import React, { useState } from "react";
import Button from "../../components/Button";
import Input from "../../components/Input";
import Select from "../../components/Select";
import TextArea from "../../components/TextArea";
import useForm from "../../hooks/useForm";
import validate, { regexRule, requireRule } from "../../utils/validate";

const rules = {
  name: [requireRule("Vui lòng nhập name")],
  email: [
    requireRule("Vui lòng nhập email"),
    regexRule("Vui lòng nhập đúng định dạng email", "email"),
  ],
  phone: [
    requireRule("Vui lòng nhập phone"),
    regexRule("Vui lòng nhập đúng định dạng phone", "phone"),
  ],
  topic: [requireRule("Vui lòng nhập topic")],
  content: [requireRule("Vui lòng nhập content")],
};

const ContactForm = ({ handleFormSubmit }) => {
  const { form, error, register, validate } = useForm(
    {
      name: "",
      email: "",
      phone: "",
      topic: "",
      content: "",
    },
    rules
  );

  const _onSubmit = () => {
    const errorObject = validate();
    if (Object.keys(errorObject).length > 0) {
      console.log("Submit Error :>> ", errorObject);
    } else {
      handleFormSubmit?.(form);
    }
  };

  return (
    <div className="form">
      <h3 className="title --t3">Gửi yêu cầu hỗ trợ</h3>
      <Input
        label="Họ và tên"
        required
        placeholder="Họ và tên"
        {...register("name")}
      />
      <Input
        label="Email"
        required
        placeholder="Email"
        {...register("email")}
      />
      <Input
        label="Phone"
        required
        placeholder="Phone"
        {...register("phone")}
      />
      <Input
        label="Chủ đề cần hỗ trợ "
        required
        renderInput={(inputProps) => {
          return (
            <Select
              options={[
                { value: "", label: "---" },
                { value: "react", label: "ReactJs" },
                { value: "responsive", label: "Web Responsive" },
              ]}
              {...inputProps}
            />
          );
        }}
        {...register("topic")}
      />
      <Input
        label="Nội dung"
        required
        renderInput={(inputProps) => {
          return <TextArea {...inputProps} />;
        }}
        {...register("content")}
      />
      <div className="btncontrol">
        <Button variant="primary" onClick={_onSubmit}>
          Gửi
        </Button>
      </div>
    </div>
  );
};

export default ContactForm;
