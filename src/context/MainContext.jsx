/* eslint-disable react/prop-types */
/* eslint-disable react-refresh/only-export-components */
import { useContext, createContext, useState, useEffect } from "react";
import { useLocation } from "react-router-dom";

const MainContext = createContext({});

const MainContextProvider = ({ children }) => {
  const { pathname } = useLocation();
  const [isShowNavbar, setIsShowNavbar] = useState(false);

  useEffect(() => {
    setIsShowNavbar(false);

    const myTimeout = setTimeout(() => {
      // scrollToTop
      window.scrollTo({
        top: 0,
        left: 0,
        behavior: "auto",
      });
    }, 100);
    return () => {
      clearTimeout(myTimeout);
    };
  }, [pathname]);

  const handleShowNavbar = (isShow) => {
    setIsShowNavbar(isShow);
  };

  return (
    <MainContext.Provider value={{ isShowNavbar, handleShowNavbar }}>
      {children}
    </MainContext.Provider>
  );
};

export default MainContextProvider;

export const useMainContext = () => useContext(MainContext);
