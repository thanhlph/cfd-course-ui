/* eslint-disable react/prop-types */
/* eslint-disable no-unused-vars */
import { message } from "antd";
import React, { createContext, useContext, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import PATHS from "../constants/path";
import { authService } from "../services/authService";
import { orderService } from "../services/orderService";
import tokenMethod from "../utils/token";
const AuthContext = createContext({});

const AuthContextProvider = ({ children }) => {
  const [showedModal, setShowedModal] = useState("");
  const navigate = useNavigate();
  const [profile, setProfile] = useState({});
  const [courseInfo, setCourseInfo] = useState([]);
  const [paymentInfo, setPaymentInfo] = useState([]);

  useEffect(() => {
    const accessToken = !!tokenMethod.get()?.accessToken;
    if (accessToken) {
      handleGetProfile();
      handleGetProfileCourse();
      handleGetProfilePayment();
    }
  }, []);

  const handleShowModal = (modalType) => {
    setShowedModal(modalType || "");
  };

  const handleCloseModal = (e) => {
    e?.stopPropagation();
    setShowedModal("");
  };

  const handleLogin = async (loginData, callback) => {
    // Xu ly payload
    const payload = { ...loginData };
    // Xu ly API login
    try {
      const res = await authService.login(payload);
      console.log("res :>> ", res);
      if (res?.data?.data) {
        const { token: accessToken, refreshToken } = res.data.data || {};
        message.success("Đăng nhập thành công");

        // Luu Token
        // 1. LocalStorage && 2. Luu = Cookie
        tokenMethod.set({
          accessToken,
          refreshToken,
        });
        // Lay thong tin profile
        handleGetProfile();
        // Dong modal & Thong bao thanh cong
        handleCloseModal();
      } else {
        message.error("Đăng nhập thất bại");
      }
    } catch (error) {
      console.log("error", error);
    } finally {
      callback?.();
    }
  };

  const handleRegister = async (registerData, callback) => {
    const { name, email, password } = registerData || {};

    // Xu ly payload

    const payload = {
      firstName: name,
      lastName: "",
      email,
      password,
    };

    console.log("payload :>> ", payload);

    // Xu ly API Register
    try {
      const res = await authService.register(payload);
      console.log("res", res);
      if (res?.data?.data?.id) {
        // handle Login
        handleLogin({
          email,
          password,
        });

        // Thong bao
        message.success("Đăng ký thành công");
      } else {
        message.error("Đăng ký thất bại");
      }
    } catch (error) {
      console.log("error :>> ", error);
      message.error("Đăng ký thất bại");
    } finally {
      callback?.();
    }
  };

  const handleLogout = () => {
    tokenMethod.remove();
    navigate(PATHS.HOME);
    message.success("Tài khoản đã đăng xuất");
  };

  const handleGetProfile = async () => {
    // Call API get Profile
    try {
      const res = await authService.getProfile();
      if (res?.data?.data) {
        setProfile(res.data.data);
      }
    } catch (error) {
      console.log("error", error);
      handleLogout();
    }
  };
  const handleGetProfileCourse = async () => {
    try {
      const res = await orderService.getCourseHistories();
      const orderedCourses = res?.data?.data?.orders || [];
      setCourseInfo(orderedCourses);
    } catch (error) {
      console.log("getCourseHistories error", error);
    }
  };

  const handleGetProfilePayment = async () => {
    try {
      const res = await orderService.getPaymentHistories();
      const payments = res?.data?.data?.orders || [];
      setPaymentInfo(payments);
    } catch (error) {
      console.log("getPaymentHistories error", error);
    }
  };

  const handleUpdateProfile = async (profileData) => {
    try {
      const {
        firstName,
        email,
        password,
        facebookURL,
        introduce,
        phone,
        website,
      } = profileData;

      const payload = {
        firstName,
        lastName: "",
        email,
        password,
        facebookURL,
        introduce,
        phone,
        website,
      };

      const res = await authService.updateProfile(payload);
      if (res?.data?.data?.id) {
        message.success("Cập nhật thành công");
        handleGetProfile();
      }
    } catch (error) {
      console.log("error", error);
      message.error("Cập nhật thất bại");
    }
  };

  return (
    <AuthContext.Provider
      value={{
        profile,
        showedModal,
        courseInfo,
        paymentInfo,
        handleShowModal,
        handleCloseModal,
        handleLogin,
        handleRegister,
        handleLogout,
        handleGetProfileCourse,
        handleGetProfilePayment,
        handleUpdateProfile,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export default AuthContextProvider;

export const useAuthContext = () => useContext(AuthContext);
